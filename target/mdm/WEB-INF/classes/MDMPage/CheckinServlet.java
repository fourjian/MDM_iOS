package MDMPage;
//import com.notnoop.apns.APNS;
//import com.notnoop.apns.ApnsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 * Created by tao on 2017/7/25.
 */
@WebServlet(name = "CheckinServlet")
public class CheckinServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setHeader("content-type", "application/xml;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        String configTitle = "MDMApp_EraseDevice";
        response.setHeader("Content-Disposition", "attachment; filename=" + configTitle + ".plist");
    }

    protected void doPut(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        res.setContentType("text/html;charset=UTF-8");

//        this.sendMsg();


        BufferedReader br = new BufferedReader(new InputStreamReader(req.getInputStream()));

        String s = null;

        while((s = br.readLine()) != null) {
            System.out.println(s);
        }

        if( br != null ){
            br.close();
        }

    }

//    public void sendMsg () {
//
//        System.out.print("doSendMsg");
//
//        ApnsService service = APNS.newService().withCert("/MDMPage/server.p12","Wwhosyour123").withSandboxDestination().build();
//        String payload = APNS.newPayload().alertBody("Can't be simpler than this!").build();
//        String token = "Uug5ZrpCrnDmLv2ywIWcroEXnn2dWszC0LFI38WGDFo=";
//        service.push(token,payload);
//        Map<String, Date> inactiveDevices = service.getInactiveDevices();
//        for (String deviceToken : inactiveDevices.keySet()) {
//            Date inactiveAsOf = inactiveDevices.get(deviceToken);
//        }
//
//    }



    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.print("doGet");
        System.out.print("doGet");
        System.out.print("doGet");
        System.out.print("doGet");
    }
}