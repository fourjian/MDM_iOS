package MDMPage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;


/**
 * Created by tao on 2017/7/26.
 */
public class mdmServlet extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    public void deviceLock() throws Exception {
        System.out.println("-------------------Lock Start---------------");
        //返回到页面
        Map map = new HashMap();
        /**获取设备ID查询设备的MDM推送相关的Token、pushMagic等信息**/
        String deviceId = ControllerContext.getContext().getPathParamMap().get("deviceId");
        DBService dbService = ControllerContext.getContext().getDBService(Mdm.class);
        Mdm mdm = (Mdm) dbService.find("deviceId = ?", new Object[]{deviceId});
        /**获取p12格式的MDM推送证书路径，对设备进行锁屏**/
        String pemFile = PushConfig.getConfig("APNS_P12MDM");
        HttpServletRequest request = (HttpServletRequest) ControllerContext.getContext().getHttpRequest();
        String pemPath = request.getRealPath("mdmtool") + pemFile;
        int pushState = PushUtils.singleMDMLockPush(pemPath, mdm);
        if (pushState == 1) {
            DBService commandService = ControllerContext.getContext().getDBService(Command.class);
            Command command = new Command();
            command.setCommand(MdmUtils.Lock);
            command.setDeviceId(deviceId);
            command.setDoIt("0");
            commandService.saveOrUpdate(command);
            map.put("state", "1");
            map.put("msg", "锁屏命令发送成功");
        } else {
            map.put("state", "0");
            map.put("msg", "锁屏命令发送失败");
        }
        System.out.println("-------------------Lock End---------------");
        toResult(Response.Status.OK.getStatusCode(), map);
    }

}
