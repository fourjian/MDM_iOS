MDM开发步骤总结如下：

1，证书的准备

1.1、申请iOS开发MDM证书	

1.1.1、MDM Vendor申请（必须是企业版开发者账号）	

1.1.2、vendor资格申请好之后，开始制作vendor证书（mdm.cer）	

1.1.3、作为一个customer（申请apple推送证书）	

1.1.3.1、创建MDM推送CSR	

1.1.3.2、导出私钥并制作证书	

1.1.3.3、使用mdm_vendor_sign工具制作CSR	

1.1.3.4、从Apple获取推送证书	

1.2、申请MDM需要的https(SSL)证书和密钥（大坑）	

1.2.1、网络上的教程	

1.2.1.1 自签名SSL证书（参考http://www.jianshu.com/p/2173e0df5761）	

1.2.1.2 StartSSL网站申请免费SSL证书（http://www.startssl.com）	

1.2.2、我所使用的方法	

2，服务端的搭建	

2.1、安装和部署maven	

2.2、安装和部署Tomcat	

2.3、新建两个servlet，并且实现doPut方法	

2.3.1 CheckinServlet，用来接收和处理设备安装描述文件时候提交的xml数据。	

2.3.2 mdmServlet，用来接收和处理设备被唤醒后连接服务器时提交的xml数据，并且返还plist文件指令控制设备。	

2.4、在合适的地方，调用sendMsg方法	

3，配置文件	

3.1、生成和安装的流程	

3.1.1生成配置mobileconfig文件	

3.2、凭证问题（大坑）	

3.3、移动设备管理（大坑）	

4.总结	

详细过程已经放在项目的MDMFile文件夹里