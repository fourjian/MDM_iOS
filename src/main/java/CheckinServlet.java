//import com.notnoop.apns.APNS;
//import com.notnoop.apns.ApnsService;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import org.slf4j.impl.StaticLoggerBinder;
import org.xml.sax.XMLReader;
import sun.misc.BASE64Decoder;


/**
 * Created by tao on 2017/7/25.
 */
@WebServlet(name = "CheckinServlet")
public class CheckinServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setHeader("content-type", "application/xml;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        String configTitle = "MDMApp_EraseDevice";
        response.setHeader("Content-Disposition", "attachment; filename=" + configTitle + ".plist");
    }

    protected void doPut(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        res.setContentType("text/html;charset=UTF-8");

        BufferedReader br = new BufferedReader(new InputStreamReader(req.getInputStream()));

        String s = null;

        while((s = br.readLine()) != null) {
            System.out.println(s);
        }

        if( br != null ){
            br.close();
        }

    }

    public static String parseToken(String OriToken) throws IOException{
        BASE64Decoder decoder = new BASE64Decoder();
        byte[] decodedBytes = decoder.decodeBuffer(OriToken);
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < decodedBytes.length; ++i) {
            buf.append(String.format("%02x", decodedBytes[i]));
        }
        String Token = buf.toString();
        return  Token;
    }
    public void sendMsgToAPNS () throws IOException {
        ///暂时写死发送通知需要的参数

        // p12文件的本地路径
        String certPath = "/Users/tao/IdeaProjects/MDMServer/src/main/webapp/file/forServer.p12";
        //p12文件的密码
        String certPassword = "Wwhosyour123";
        // i5真机安装描述文件时上传的pushMagic (每安装一次描述文件都会变。)
        String iPhone_FivePushMagic = "07C182AF-11EF-4AF3-A493-F6ABBBEE2CAC";
        // i6真机安装描述文件时上传的pushMagic (每安装一次描述文件都会变。)
        String iPhone_SixPushMagic = "7AB2267C-0368-4ECC-B476-CE49FD32E50A";
        // i5真机安装描述文件时上传的设备token  (每台机器唯一，但是刷机或者重装系统后会改变。)
        String iPhone_FiveToken =  "U/Y+gxLNR3CJabu8TCSYT2jrv917hvPJH5IJ2oxJ2M8=";
        // i6真机安装描述文件时上传的设备token  (每台机器唯一，但是刷机或者重装系统后会改变。)
        String iPhone_SixToken =  "Uug5ZrpCrnDmLv2ywIWcroEXnn2dWszC0LFI38WGDFo=";


        //使用方法转换一开始上传上来的token Fix:token位数错误的问题
        String token = parseToken(iPhone_SixToken);
        ApnsService service = APNS.newService().withCert(certPath,certPassword).withProductionDestination().build();
        String payload = APNS.newPayload().customField("mdm",iPhone_SixPushMagic).build();
        System.out.print("发送消息到苹果服务器");
        //发送通知到APNS
        service.push(token,payload);

    }



    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.print("doGet");
       //访问CheckinServlet的时候就发送消息到苹果服务器
        this.sendMsgToAPNS();
    }


}