import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;


/**
 * Created by tao on 2017/7/26.
 */
public class mdmServlet extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        System.out.print("postpost");
        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String s = null;

        while((s = br.readLine()) != null) {
            System.out.println(s);
        }

        if( br != null ){
            br.close();
        }
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
//        System.out.print("getget");
        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));

        String s = null;

        while((s = br.readLine()) != null) {
            System.out.println(s);
        }

        if( br != null ){
            br.close();
        }
    }

    protected void doPut(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        System.out.print("\n设备主动连接服务器\n");
//
//        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
//
//        String s = null;
//
//        while((s = br.readLine()) != null) {
//            System.out.println(s);
//        }

//        if( br != null ){
//            br.close();
//        }




        SAXReader reader = new SAXReader();
        try {
            Document doc = reader.read(request.getInputStream());
            Element root = doc.getRootElement();
            Element dict = root.element("dict");
            List<Element> strings = dict.elements("string");
            StringWriter sw = new StringWriter();
            OutputFormat format = OutputFormat.createPrettyPrint();
            XMLWriter xw = new XMLWriter(sw, format);
            xw.write(doc);
            String result = sw.toString();
            System.out.println(result);
            for (Element ele : strings) {


                String text = ele.getText();
                System.out.println("遍历strings\n");
                if ("Idle".equals(text)){
                    System.out.println("遍历到了Idle字段，说明设备处于空闲状态，执行指令\n");
//各类指令
                    //锁屏
//                    String deviceLock = prepareDeviceLockPlist();
                    //安装app
//                    String installApp = prepareInstallAppPlist();
                    //删除app
//                    String removeApp = prepareRemoveAppPlist();
                    //查询安装了的app
                    String queryInstalledAppPlist = prepareQueryInstalledAppPlist();
                    //抹除数据(慎用  结尾删了个字母t)
//                    String eraseDevice = prepareErasePlis();
                    //查询设备数据
//                    String queryDevice = prepareQueryPlist();

                    //开启丢失模式（暂时跑不起来）
//                    String openLostMode = prepareLostModePlist();
                    //播放警报声（暂时跑不起来）
//                    String showWarningVoice = prepareWarningVoicePlist();

                    response.setHeader("content-type", "application/xml;charset=UTF-8");
                    response.setCharacterEncoding("UTF-8");
                    String configTitle = "Mbaike_DeviceLock";
                    response.setHeader("Content-Disposition", "attachment; filename=" + configTitle + ".plist");
                    PrintWriter sos = response.getWriter();
                    System.out.println("指令完成\n");

                    sos.write(queryInstalledAppPlist);
                    sos.flush();
                    sos.close();
                    System.out.println("关闭写入流\n");

                }
                else {
                    System.out.println("未遍历到Idle字段");
                    return;
                }
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }
    //安装应用程序
    public String prepareInstallAppPlist() {

        StringBuffer backString = new StringBuffer();
        backString.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        backString.append("<!DOCTYPE plist PUBLIC \"-//Apple Computer//DTD PLIST 1.0//EN\" \n");
        backString.append("\"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\n");
        backString.append("<plist version=\"1.0\">\n");
        backString.append("<dict>\n");
        backString.append("<key>Command</key>\n" );
        backString.append("<dict>\n");
        backString.append("<key>RequestType</key>\n");
        backString.append("<string>InstallApplication</string>\n");
        backString.append("<key>iTunesStoreID</key>");
        backString.append("<integer>450748070</integer>");
        backString.append("<key>ManagementFlags</key>\n");
        backString.append("<integer>1</integer>\n");
        backString.append("</dict>\n");
        backString.append("<key>CommandUUID</key>\n");
        backString.append("<string>07a6c20e-5e35-4f79-8680-10dee8460098</string>\n");
        backString.append("</dict>\n");
        backString.append("</plist>");
        return backString.toString();
    }
    //删除应用程序
    public String prepareRemoveAppPlist() {

        StringBuffer backString = new StringBuffer();
        backString.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        backString.append("<!DOCTYPE plist PUBLIC \"-//Apple Computer//DTD PLIST 1.0//EN\" \n");
        backString.append("\"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\n");
        backString.append("<plist version=\"1.0\">\n");
        backString.append("<dict>\n");
        backString.append("<key>Command</key>\n" );
        backString.append("<dict>\n");
        backString.append("<key>RequestType</key>\n");
        backString.append("<string>RemoveApplication</string>\n");
        backString.append("<key>Identifier\n</key>");
        backString.append("<string>com.youdao.note.iphone</string>");
        backString.append("</dict>\n");
        backString.append("<key>CommandUUID</key>\n");
        backString.append("<string>07a6c20e-5e35-4f79-8680-10dee8460098</string>\n");
        backString.append("</dict>\n");
        backString.append("</plist>");
        return backString.toString();
    }
    //设备锁屏
    public String prepareDeviceLockPlist() {

        StringBuffer backString = new StringBuffer();
        backString.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        backString.append("<!DOCTYPE plist PUBLIC \"-//Apple Computer//DTD PLIST 1.0//EN\"");
        backString.append("\"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
        backString.append("<plist version=\"1.0\"><dict><key>Command</key><dict><key>RequestType</key><string>");
        backString.append("DeviceLock");
        backString.append("</string><key>Message</key><string>");
        backString.append("byebye");
        backString.append("</string><key>PhoneNumber</key><string>");
        backString.append("15603019583");
        backString.append("</string></dict><key>CommandUUID</key><string>");
        backString.append("07a6c20e-5e35-4f79-8680-10dee8460099");
        backString.append("</string></dict></plist>");
        return backString.toString();
    }
    //查询设备已经安装的app列表
    public String prepareQueryInstalledAppPlist() {

        StringBuffer backString = new StringBuffer();
        backString.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        backString.append("<!DOCTYPE plist PUBLIC \"-//Apple Computer//DTD PLIST 1.0//EN\" \n");
        backString.append("\"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\n");
        backString.append("<plist version=\"1.0\">\n");
        backString.append("<dict>\n");
        backString.append("<key>Command</key>\n" );
        backString.append("<dict>\n");
        backString.append("<key>RequestType</key>\n");
        backString.append("<string>InstalledApplicationList</string>\n");
        backString.append("<key>Identifiers\n</key>");
        backString.append("<array>\n");
        backString.append("<string>com.youdao.note.iphone</string>");
        backString.append("</array>\n");
        backString.append("</dict>\n");
        backString.append("<key>CommandUUID</key>\n");
        backString.append("<string>07a6c20e-5e35-4f79-8680-10dee8460098</string>\n");
        backString.append("</dict>\n");
        backString.append("</plist>");
        return backString.toString();
    }
    //开启丢失模式
    public String prepareLostModePlist(){



        StringBuffer backString = new StringBuffer();
        backString.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        backString.append("<!DOCTYPE plist PUBLIC \"-//Apple Computer//DTD PLIST 1.0//EN\"");
        backString.append("\"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
        backString.append("<plist version=\"1.0\">");
        backString.append("<dict>");
        backString.append("<key>Command</key>");
        backString.append("<dict>");
        backString.append("<key>RequestType</key>");
        backString.append("<string>EnableLostMode</string>");
        backString.append("<key>Message</key>");
        backString.append("<string>");
        backString.append("把我的手机还给我");
        backString.append("</string>");
        backString.append("</dict>");
        backString.append("<key>CommandUUID</key>");
        backString.append("<string>");
        backString.append("07a6c20e-5e35-4f79-8680-10dee8460099");
        backString.append("</string>");
        backString.append("</dict>");
        backString.append("</plist>");
        return backString.toString();
    }
    //发出警报声
    public String prepareWarningVoicePlist () {


        StringBuffer backString = new StringBuffer();
        backString.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        backString.append("<!DOCTYPE plist PUBLIC \"-//Apple Computer//DTD PLIST 1.0//EN\"");
        backString.append("\"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
        backString.append("<plist version=\"1.0\">");
        backString.append("<dict>");
        backString.append("<key>Command</key>");
        backString.append("<dict>");
        backString.append("<key>RequestType</key>");
        backString.append("<string>");
        backString.append("DeviceLocation");
        backString.append("</string>");
        backString.append("</dict>");
        backString.append("<key>CommandUUID</key>");
        backString.append("<string>");
        backString.append("07a6c20e-5e35-4f79-8680-10dee8460099");
        backString.append("</string>");
        backString.append("</dict>");
        backString.append("</plist>");
        return backString.toString();

    }

    //抹除设备数据
    public String prepareErasePlist () {


        StringBuffer backString = new StringBuffer();
        backString.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        backString.append("<!DOCTYPE plist PUBLIC \"-//Apple Computer//DTD PLIST 1.0//EN\"");
        backString.append("\"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
        backString.append("<plist version=\"1.0\">");
        backString.append("<dict>");
        backString.append("<key>Command</key>");
        backString.append("<dict>");
        backString.append("<key>RequestType</key>");
        backString.append("<string>");
        backString.append("EraseDevice");
        backString.append("</string>");
        backString.append("<key>PIN</key>");
        backString.append("<string>");
        backString.append("Wwhosyour123");
        backString.append("</string>");
        backString.append("</dict>");
        backString.append("<key>CommandUUID</key>");
        backString.append("<string>");
        backString.append("07a6c20e-5e35-4f79-8680-10dee8460099");
        backString.append("</string>");
        backString.append("</dict>");
        backString.append("</plist>");
        return backString.toString();

    }

    //查询相关信息
    public String prepareQueryPlist () {


        StringBuffer backString = new StringBuffer();
        backString.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        backString.append("<!DOCTYPE plist PUBLIC \"-//Apple Computer//DTD PLIST 1.0//EN\"");
        backString.append("\"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
        backString.append("<plist version=\"1.0\">");
        backString.append("<dict>");
        backString.append("<key>Command</key>");
        backString.append("<dict>");
        backString.append("<key>RequestType</key>");
        backString.append("<string>");
        backString.append("DeviceInformation");
        backString.append("</string>");
        backString.append("<key>Queries</key>");
        backString.append("<array>");
        backString.append("<string>ModelName</string>");
        backString.append("<string>Model</string>");
        backString.append("<string>BatteryLevel</string>");
        backString.append("<string>DeviceCapacity</string>");
        backString.append("<string>AvailableDeviceCapacity</string>");
        backString.append("<string>OSVersion</string>");
        backString.append("<string>SerialNumber</string>");
        backString.append("<string>IMEI</string>");
        backString.append("<string>ICCID</string>");
        backString.append("<string>MEID</string>");
        backString.append("<string>IsSupervised</string>");
        backString.append("<string>IsDeviceLocatorServiceEnabled</string>");
        backString.append("<string>IsActivationLockEnabled</string>");
        backString.append("<string>IsCloudBackupEnabled</string>");
        backString.append("<string>WiFiMAC</string>");
        backString.append("<string>BluetoothMAC</string>");
        backString.append("</array>");
        backString.append("</dict>");
        backString.append("<key>CommandUUID</key>");
        backString.append("<string>");
        backString.append("07a6c20e-5e35-4f79-8680-10dee8460099");
        backString.append("</string>");
        backString.append("</dict>");
        backString.append("</plist>");
        return backString.toString();

    }

}
